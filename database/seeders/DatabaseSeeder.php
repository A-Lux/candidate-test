<?php

namespace Database\Seeders;

use App\Contracts\ProductContract;
use App\Contracts\ProductFilialContract;
use App\Models\Category;
use App\Models\Country;
use App\Models\Filial;
use App\Models\Product;
use App\Models\ProductFilial;
use App\Models\Type;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        print_r("Категории добавляются\n");
        $this->command->getOutput()
                      ->progressStart(300)
        ;
        Category::factory(300)
                ->create()->each(function ($amount) {
                $this->command->getOutput()->progressAdvance();
            });
        ;
        $this->command->getOutput()
                      ->progressFinish()
        ;
        print_r("Категории добавлены\n");
        print_r("Страны добавляются\n");
        $this->command->getOutput()
                      ->progressStart(10)
        ;
        Country::factory(10)
               ->create()->each(function ($amount) {
                $this->command->getOutput()->progressAdvance();
            });
        ;
        $this->command->getOutput()
                      ->progressFinish()
        ;
        print_r("Страны добавлены\n");
        print_r("Типы добавляются\n");
        $this->command->getOutput()
                      ->progressStart(15)
        ;
        Type::factory(15)
            ->create()->each(function ($amount) {
                $this->command->getOutput()->progressAdvance();
            })
        ;
        $this->command->getOutput()
                      ->progressFinish()
        ;
        print_r("Типы добавлены\n");
        print_r("Продукты добавляются\n");
        $products = json_decode(file_get_contents(dirname(__DIR__, 1) . '/products.json'));
        $this->command->getOutput()
                      ->progressStart(count($products))
        ;
        $categoryIDs = Category
            ::query()
            ->select('id')
            ->get();
        $countryIDs = Country
            ::query()
            ->select('id')
            ->get();
        $typeIDs = Type
            ::query()
            ->select('id')
            ->get();
        $productsData = [];
        foreach ($products as $product) {
            $productsData[] = [
                ProductContract::TITLE       => $product->name,
                ProductContract::IMAGE       => 'https://picsum.photos/200/300',
                ProductContract::CATEGORY_ID => $categoryIDs->random()->id,
                ProductContract::COUNTRY_ID  => $countryIDs->random()->id,
                ProductContract::PRICE       => random_int(500, 50000),
                ProductContract::TYPE_ID     => $typeIDs->random()->id,
                ProductContract::CREATED_AT  => now()->toDateTimeString(),
                ProductContract::UPDATED_AT  => now()->toDateTimeString(),
            ];
        }
        $chunks = array_chunk($productsData, 5000);
        foreach ($chunks as $chunk){
            Product::query()->insert($chunk);
            $this->command->getOutput()
                          ->progressAdvance()
            ;
        }
        $this->command->getOutput()
                      ->progressFinish()
        ;

        print_r("Продукты добавлены\n");
        print_r("Филиалы добавляются\n");
        $this->command->getOutput()
                      ->progressStart(15)
        ;
        Filial::factory(15)
              ->create()
              ->each(
                  function ($amount) {
                      $this->command->getOutput()
                                    ->progressAdvance()
                      ;
                  }
              )
        ;
        $this->command->getOutput()
                      ->progressFinish()
        ;
        print_r("Филиалы добавлены\n");

        $amounts = [];
        print_r("Собирается информация об остатках\n");
        $this->command->getOutput()
                      ->progressStart(50000)
        ;
        $filialIDs = Filial
            ::query()
            ->select('id')
            ->get()
        ;
        $productIDs = Filial
            ::query()
            ->select('id')
            ->get()
        ;
        for ($i = 0; $i < 50000; $i++) {
            $amounts[] = [
                ProductFilialContract::FILIAL_ID  => $filialIDs->random()->id,
                ProductFilialContract::PRODUCT_ID => $productIDs->random()->id,
                ProductFilialContract::AMOUNT     => random_int(0, 2000),
                ProductFilialContract::CREATED_AT => now()->toDateTimeString(),
                ProductFilialContract::UPDATED_AT => now()->toDateTimeString(),
            ];
            $this->command->getOutput()
                          ->progressAdvance()
            ;
        }

        $this->command->getOutput()
                      ->progressFinish()
        ;
        $chunks = array_chunk($amounts, 5000);
        print_r("Информация об остатках собрана\n");
        print_r("Остатки добавляются\n");
        $this->command->getOutput()
                      ->progressStart(50000)
        ;

        foreach ($chunks as $chunk){
            ProductFilial::query()->insert($chunk);
            $this->command->getOutput()
                          ->progressAdvance()
            ;
        }

        $this->command->getOutput()
                      ->progressFinish()
        ;
        print_r("Остатки добавлены\n");
    }
}
