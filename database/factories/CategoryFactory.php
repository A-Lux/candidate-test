<?php

namespace Database\Factories;

use App\Contracts\CategoryContract;
use App\Models\Category;
use Illuminate\Database\Eloquent\Factories\Factory;

class CategoryFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Category::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $types = collect(json_decode(file_get_contents(dirname(__DIR__, 1) . '/products.json')))->pluck('name');

        $type = $types->random();
        return [
            CategoryContract::TITLE => $type
        ];
    }
}
