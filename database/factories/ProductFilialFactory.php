<?php

namespace Database\Factories;

use App\Contracts\ProductFilialContract;
use App\Models\Filial;
use App\Models\Product;
use App\Models\ProductFilial;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFilialFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ProductFilial::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            ProductFilialContract::FILIAL_ID  => Filial
                ::query()
                ->inRandomOrder()
                ->first()->id,
            ProductFilialContract::PRODUCT_ID => Product
                ::query()
                ->inRandomOrder()
                ->first()->id,
            ProductFilialContract::AMOUNT     => random_int(0, 2000),
        ];
    }
}
