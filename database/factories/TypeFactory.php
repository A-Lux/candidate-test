<?php

namespace Database\Factories;

use App\Contracts\TypeContract;
use App\Models\Type;
use Illuminate\Database\Eloquent\Factories\Factory;

class TypeFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Type::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $types = collect([
            'Жидкость',
            'Таблетки',
            'Капсулы',
            'Мазь',
            'Порошок',
            'Спрей',
            'Инъекция',
            'Имплант',
        ]);
        return [
            TypeContract::TITLE => $types->random()
        ];
    }
}
