<?php

namespace Database\Factories;

use App\Contracts\FilialContract;
use App\Models\Filial;
use Illuminate\Database\Eloquent\Factories\Factory;

class FilialFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Filial::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            FilialContract::TITLE => $this->faker->city
        ];
    }
}
