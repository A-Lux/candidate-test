<?php

use App\Contracts\ProductContract;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(ProductContract::TABLE, function (Blueprint $table) {
            $table->id();
            $table->string(ProductContract::TITLE);
            $table->string(ProductContract::IMAGE);
            $table->foreignId(ProductContract::CATEGORY_ID)->constrained();
            $table->foreignId(ProductContract::COUNTRY_ID)->constrained();
            $table->integer(ProductContract::PRICE)->unsigned();
            $table->foreignId(ProductContract::TYPE_ID)->constrained();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(ProductContract::TABLE);
    }
}
