<?php

use App\Contracts\ProductFilialContract;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductFilialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            ProductFilialContract::TABLE, function (Blueprint $table) {
            $table->id();
            $table->foreignId(ProductFilialContract::PRODUCT_ID)->constrained();
            $table->foreignId(ProductFilialContract::FILIAL_ID)->constrained();
            $table->integer(ProductFilialContract::AMOUNT)->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(ProductFilialContract::TABLE);
    }
}
