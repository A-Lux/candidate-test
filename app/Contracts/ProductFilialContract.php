<?php

namespace App\Contracts;

interface ProductFilialContract
{
    public const TABLE      = 'product_filial';
    public const PRODUCT_ID = 'product_id';
    public const FILIAL_ID  = 'filial_id';
    public const AMOUNT     = 'amount';
    public const CREATED_AT = 'created_at';
    public const UPDATED_AT = 'updated_at';
}
