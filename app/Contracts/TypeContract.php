<?php

namespace App\Contracts;

interface TypeContract
{
    public const TABLE = 'types';
    public const TITLE = 'title';
}
