<?php

namespace App\Contracts;

interface CategoryContract
{
    public const TABLE = 'categories';
    public const TITLE = 'title';
}
