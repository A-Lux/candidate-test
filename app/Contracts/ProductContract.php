<?php

namespace App\Contracts;

interface ProductContract
{
    public const TABLE       = 'products';
    public const TITLE       = 'title';
    public const IMAGE       = 'image';
    public const CATEGORY_ID = 'category_id';
    public const COUNTRY_ID  = 'country_id';
    public const PRICE       = 'price';
    public const TYPE_ID     = 'type_id';
    public const CREATED_AT = 'created_at';
    public const UPDATED_AT = 'updated_at';
}
