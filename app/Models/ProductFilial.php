<?php

namespace App\Models;

use App\Contracts\ProductFilialContract;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductFilial extends Model
{
    use HasFactory;

    protected $table = ProductFilialContract::TABLE;
}
